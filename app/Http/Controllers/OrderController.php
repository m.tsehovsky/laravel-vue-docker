<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('orderItems')->paginate();

        return OrderResource::collection($orders);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('orderItems')->find($id);

        return new OrderResource($order);
    }

   public function export()
   {
       $headers = [
           "Content-type"           => "text/csv",
           "Content-Disposition"    => "attachment; filename=orders.csv",
           "Pragma"                 => "no-cache",
           "Cache-Control"          => "must-revalidate, post-check=0, pre-check=0",
           "Expires"                => "0",
       ];

       $callback = function () {
           $orders = Order::all();
           $file = fopen('php://output', 'w');

           fputcsv($file, ['ID', 'Name', 'Email', 'Product Title', 'Price', 'Quantity']);

           foreach ($orders as $order) {
               fputcsv($file, [$order->id, $order->name, $order->email, '', '', '']);

               foreach ($order->orderItems as $orderItem)
               fputcsv($file, ['', '', '', $orderItem->product_title, $orderItem->price, $orderItem->quantity]);
           }

           fclose($file);
       };

       return \Response::stream($callback, 200, $headers);

   }

   public function chart()
   {
//       select date_format(o.created_at, '%Y-%m-%d') as date, sum(oi.price * oi.quantity) as sum
//          from orders o
//          join order_items oi on o.id = oi.order_id
//          group by date

       return Order::query()        //It's the same with return Order::table('orders')
           ->join('order_items', 'orders.id', '=', 'order_items.order_id')
           ->selectRaw("date_format(orders.created_at, '%Y-%m-%d') as date, sum(order_items.price * order_items.quantity) as sum")
           ->groupBy('date')
           ->get();

   }
}
